# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.2.1](https://gitlab.com/abraao.as3/dango-test/compare/v1.2.0...v1.2.1) (2022-02-28)

### Bug Fixes

- publishing errors ([5bc6f7a](https://gitlab.com/abraao.as3/dango-test/commit/5bc6f7a1cc06be323a6e48287bfc233544b14337))

## [1.2.0](https://gitlab.com/abraao.as3/dango-test/compare/v1.1.0...v1.2.0) (2022-02-28)

### Features

- card context added ([858724e](https://gitlab.com/abraao.as3/dango-test/commit/858724e2d81cd5a350ca32ef4b7c123153ac284c))
- detail page ([a35f725](https://gitlab.com/abraao.as3/dango-test/commit/a35f725f76d6fbe2367fe9ec75fcfcd54b820511))
- total items ([5960bb9](https://gitlab.com/abraao.as3/dango-test/commit/5960bb9c4886a1c11ee7bcab4895e4a060ea7ef9))

## [1.1.0](https://gitlab.com/abraao.as3/dango-test/compare/v1.0.0...v1.1.0) (2022-02-28)

### Features

- card component added ([f42d099](https://gitlab.com/abraao.as3/dango-test/commit/f42d0998af61c734e10f05d69d073a65d0447f08))
- first page ([faf2e23](https://gitlab.com/abraao.as3/dango-test/commit/faf2e23918b9fdd95f850dad7a04c9eb62260b30))

## 1.0.0 (2022-02-28)

### Features

- next project added ([ca1ab72](https://gitlab.com/abraao.as3/dango-test/commit/ca1ab72715772a753407fa4f65305d7b0b38df15))
