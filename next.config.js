/* eslint-disable */
const ESLintPlugin = require('eslint-webpack-plugin');

module.exports = {
  outDir: 'build',
  webpack5: true,
  webpack(config) {

    config.plugins.push(new ESLintPlugin({ fix: true, extensions: ['ts', 'tsx', 'js', 'jsx'] }));

    return config;
  },
};
