import { useEffect, useState, useCallback, useMemo } from "react";
import { useRouter } from "next/router";

import {
  Box,
  Button,
  CircularProgress,
  Typography,
  Slider,
  TextField,
} from "@mui/material";

import { getDetail, IData, IError } from "@services/API";

const DetailPage = () => {
  const router = useRouter();
  const { date } = router.query;

  const [data, setData] = useState<IData | null>(null);
  const [fontSizeRange, setFontSizeRange] = useState(1);
  const [newTitle, setNewTitle] = useState("");

  const titleSize = useMemo(() => {
    const initial = 1.25 * 16;
    return (initial * fontSizeRange) / 16;
  }, [fontSizeRange]);

  useEffect(() => {
    initData();
  }, []);

  useEffect(() => {
    if (data) {
      setNewTitle(data.title);
    }
  }, [data]);

  const initData = useCallback(async (): Promise<null> => {
    if (!date) return null;

    const res: IData | IError = await getDetail(date as string);

    if ((res as IError).error) {
      return null;
    }

    setData(res as IData);

    return null;
  }, [date]);

  const handleFontSize = (event: Event, newValue: number | number[]) => {
    setFontSizeRange(newValue as number);
  };

  const handleNewTitle = (event: React.ChangeEvent<HTMLInputElement>) => {
    setNewTitle(event.target.value);
  };

  if (data) {
    return (
      <Box
        className="scrollbar"
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          overflowY: "auto",
        }}
      >
        <Box
          sx={{
            display: "flex",
            width: 1,
            height: "100px",
            minHeight: "100px",
            p: "10px",
          }}
        >
          <Box
            sx={{
              display: "flex",
              bgcolor: "#2E353E",
              border: `2px solid #49525F`,
              borderRadius: "4px",
              width: 1,
              px: "1rem",
              gap: 2,
              alignItems: "center",
            }}
          >
            <Box
              sx={{
                display: "flex",
                gap: 2,
                width: "33%",
                alignItems: "center",
              }}
            >
              <Typography sx={{ color: "#fff", width: "33%" }}>
                Title size:{" "}
              </Typography>
              <Slider
                value={fontSizeRange}
                min={1}
                max={5}
                onChange={handleFontSize}
              />
            </Box>
            <Box sx={{ display: "flex", flexGrow: 1 }}>
              <TextField
                variant="outlined"
                label="New Title"
                color="primary"
                fullWidth
                value={newTitle}
                onChange={handleNewTitle}
                sx={{
                  "& .MuiOutlinedInput-input": {
                    color: "#fff",
                  },
                  "& .MuiOutlinedInput-notchedOutline": {
                    borderColor: "#1FD6D0!important",
                  },
                  "& .MuiInputLabel-root": {
                    color: "#1FD6D0",
                  },
                }}
              />
            </Box>
          </Box>
        </Box>
        <Box
          sx={{
            width: "100%",
            height: "300px",
            "& img": {
              width: "100%",
              height: "100%",
              objectFit: "cover",
            },
          }}
        >
          <img src={data.url} alt={data.copyright} />
        </Box>
        <Typography
          variant="h6"
          sx={{
            width: 1,
            my: "3rem",
            color: "#fff",
            px: "1rem",
            fontSize: `${titleSize}rem`,
          }}
        >
          {newTitle}
        </Typography>
        <Typography variant="body1" sx={{ color: "#fff", px: "1rem" }}>
          {data.explanation}
        </Typography>
        <Button
          variant="contained"
          color="primary"
          sx={{
            textTransform: "none",
            display: "flex",
            my: "3rem",
            color: "#fff",
          }}
          onClick={() => router.push("/")}
        >
          Voltar
        </Button>
      </Box>
    );
  }

  return (
    <Box
      sx={{
        width: 1,
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <CircularProgress />
    </Box>
  );
};

export default DetailPage;
