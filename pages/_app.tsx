import { useEffect } from "react";

import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider, createTheme, darken } from "@mui/material";
import { CacheProvider, EmotionCache } from "@emotion/react";
import createCache from "@emotion/cache";

import { Layout } from "@components/layout/Layout";
import { CardsProvider } from "@context/CardsContext";

const createEmotionCache = (): EmotionCache => createCache({ key: "css" });
const clientSideEmotionCache = createEmotionCache();

const theme = createTheme({
  palette: {
    primary: {
      main: "#1FD6D0",
    },
    background: {
      paper: "#373E48",
      default: "#292F37",
    },
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        ".scrollbar::-webkit-scrollbar-track": {
          "-webkit-box-shadow": "inset 0 0 6px rgba(0, 0, 0, 0.3)",
          "background-color": "#373E48",
        },

        ".scrollbar::-webkit-scrollbar": {
          width: "7px",
          "background-color": "#373E48",
        },

        ".scrollbar::-webkit-scrollbar-thumb": {
          "-webkit-box-shadow": "inset 0 0 6px rgba(0, 0, 0, 0.6)",
          "background-color": darken("#373E48", 0.3),
        },
      },
    },
  },
});

/* eslint-disable @typescript-eslint/explicit-module-boundary-types, @typescript-eslint/no-floating-promises, @typescript-eslint/no-unsafe-member-access */
function MyApp({
  Component,
  pageProps,
  emotionCache = clientSideEmotionCache,
}: any): JSX.Element {
  useEffect(() => {
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <CacheProvider value={emotionCache}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Layout>
          <CardsProvider>
            <Component {...pageProps} />
          </CardsProvider>
        </Layout>
      </ThemeProvider>
    </CacheProvider>
  );
}

export default MyApp;

declare global {
  namespace JSX {
    interface IntrinsicElements {
      "bolt-input": any;
    }
  }

  interface Window {
    gtag: any;
  }
}
