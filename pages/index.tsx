import { useEffect, useState } from "react";

import { Box, CircularProgress, Grid, Typography } from "@mui/material";

import { getData, IData, IError } from "@services/API";
import { Card } from "@components/cards/Card";
import { useCards } from "@context/CardsContext";

const Home = () => {
  const [data, setData] = useState<IData[] | null>(null);

  const { total } = useCards();

  useEffect(() => {
    initData();
  }, []);

  const initData = async (): Promise<null> => {
    const res: IData[] | IError = await getData();

    if ((res as IError).error) {
      return null;
    }

    setData(res as IData[]);

    return null;
  };

  if (data) {
    return (
      <>
        <Grid
          className="scrollbar"
          container
          spacing={3}
          sx={{ p: "20px", overflowY: "auto" }}
        >
          {data.map((item: IData) => (
            <Grid key={item.date} item xs={12} sm={6} md={4} lg={3}>
              <Card item={item} />
            </Grid>
          ))}
        </Grid>
        <Box
          sx={{
            display: "flex",
            height: "60px",
            minHeight: "60px",
            p: "10px",
          }}
        >
          <Box
            sx={{
              display: "flex",
              bgcolor: "#2E353E",
              border: `2px solid #49525F`,
              borderRadius: "4px",
              width: 1,
              alignItems: "center",
            }}
          >
            <Typography
              variant="body1"
              sx={{
                ml: "1rem",
                color: "#1FD6D0",
              }}
            >
              Total: ${total.toFixed(2)}
            </Typography>
          </Box>
        </Box>
      </>
    );
  }

  return (
    <Box
      sx={{
        width: 1,
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <CircularProgress />
    </Box>
  );
};

export default Home;
