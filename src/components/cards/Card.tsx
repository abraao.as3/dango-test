import { useMemo, useState, useEffect } from "react";
import { useRouter } from "next/router";

import {
  Card as MuiCard,
  CardContent,
  CardMedia,
  Typography,
  Box,
  ButtonGroup,
  Button,
  Link,
} from "@mui/material";

import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";

import { IData } from "@services/API";
import { useCards } from "@context/CardsContext";

interface CardProps {
  item: IData;
}

const Card = ({ item }: CardProps): JSX.Element => {
  const [quant, setQuant] = useState(1);

  const router = useRouter();

  const { updateStore } = useCards();

  const initialPrice: number = useMemo(
    () => Number((Math.random() * (50 - 13) + 13).toFixed(2)),
    []
  );

  const total: number = useMemo(
    () => Number((initialPrice * quant).toFixed(2)),
    [initialPrice, quant]
  );

  useEffect(() => {
    updateStore(item.date, total);
  }, [total]);

  const goToPage = () => {
    router.push(`/${item.date}`);
  };

  return (
    <MuiCard sx={{ width: 1 }}>
      <CardMedia component="img" height="250" image={item.url} />
      <CardContent
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Typography
          variant="h6"
          sx={{
            color: "#fff",
            fontWeight: "normal",
            textAlign: "left",
            width: 1,
            lineHeight: "2rem",
            minHeight: "calc(2rem * 2)",
            overflow: "hidden",
            textOverflow: "ellipsis",
            lineClamp: "2",
            "-webkit-line-clamp": "2",
            "-webkit-box-orient": "vertical",
            display: "-webkit-box",
          }}
        >
          {item.title}
        </Typography>
        <Box
          sx={{
            color: "#fff",
            display: "flex",
            gap: 2,
            alignItems: "center",
            justifyContent: "center",
            my: "4rem",
            width: "100%",
          }}
        >
          <Typography
            variant="h6"
            sx={{
              display: "flex",
              flexGrow: 1,
              textAlign: "center",
              justifyContent: "center",
            }}
          >
            ${total}
          </Typography>
          <Box sx={{ display: "flex" }}>
            <Typography
              variant="h6"
              sx={{
                py: "3px",
                px: "15px",
                textAlign: "center",
                border: "1px solid #49525F",
              }}
            >
              {quant}
            </Typography>
            <ButtonGroup
              variant="contained"
              orientation="vertical"
              disableElevation
              sx={{
                "& svg": {
                  width: "0.8rem",
                  height: "0.8rem",
                  fill: "#fff",
                },
                "& button": {
                  px: "3px",
                  py: "3px",
                  minWidth: "0!important",
                  borderTopLeftRadius: 0,
                  borderBottomLeftRadius: 0,
                },
              }}
            >
              <Button onClick={() => setQuant((q) => q + 1)}>
                <AddIcon />
              </Button>
              <Button onClick={() => setQuant((q) => (q > 1 ? q - 1 : q))}>
                <RemoveIcon />
              </Button>
            </ButtonGroup>
          </Box>
        </Box>
        <Typography
          sx={{
            fontSize: 14,
            color: "#fff",
            overflow: "hidden",
            textOverflow: "ellipsis",
            lineClamp: "4",
            "-webkit-line-clamp": "4",
            "-webkit-box-orient": "vertical",
            display: "-webkit-box",
          }}
        >
          {item.explanation}
        </Typography>
        <Button
          variant="contained"
          color="primary"
          sx={{ color: "#fff", my: "2rem", textTransform: "none" }}
        >
          Add to cart
        </Button>
        <Link component="button" onClick={goToPage}>
          Learn More
        </Link>
      </CardContent>
    </MuiCard>
  );
};

export { Card };
