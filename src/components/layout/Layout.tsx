import { Box } from "@mui/material";

const Layout = ({ children }: any): JSX.Element => (
  <Box
    sx={{
      width: "100vw",
      height: "100vh",
      display: "flex",
      flexDirection: "column",
      justifyContent: "flex-start",
      alignItems: "center",
      overflowX: "hidden",
    }}
  >
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        height: "100%",
        width: "100%",
        maxWidth: "1280px",
      }}
    >
      {children}
    </Box>
  </Box>
);

export { Layout };
