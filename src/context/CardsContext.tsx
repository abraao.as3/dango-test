import React, { createContext, useContext, useMemo, useState } from "react";

const CardContext = createContext(null);

function CardsProvider({ children }: { children: React.ReactNode }) {
  const [store, setStore] = useState<{ [key: string]: number }>({});

  const total = useMemo(() => {
    return Object.values(store).reduce((prev, curr) => prev + curr, 0);
  }, [store]);

  const updateStore = (key: string, value: number) => {
    setStore((old) => ({ ...old, [key]: value }));
  };

  return (
    <CardContext.Provider value={{ total, updateStore }}>
      {children}
    </CardContext.Provider>
  );
}

function useCards() {
  return useContext(CardContext);
}

export { CardsProvider, useCards };
