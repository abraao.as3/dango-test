import { sub, format } from "date-fns";

export interface IData {
  copyright: string;
  date: string;
  explanation: string;
  hdurl: string;
  media_type: string;
  service_version: string;
  title: string;
  url: string;
}

export interface IError {
  error: string;
}

export async function getData(): Promise<IData[] | IError> {
  const today = new Date();
  const range = sub(today, { days: 6 });
  const formatPattern = "yyyy-MM-dd";

  try {
    const data = (await fetch(
      `https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&start_date=${format(
        range,
        formatPattern
      )}&end_date=${format(today, formatPattern)}`,
      { cache: "force-cache" }
    ).then((res) => res.json())) as IData[];

    return data;
  } catch (err) {
    return { error: err } as IError;
  }
}

export async function getDetail(date: string): Promise<IData | IError> {
  try {
    return (await fetch(
      `https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&date=${date}`,
      { cache: "force-cache" }
    ).then((res) => res.json())) as IData;
  } catch (err) {
    return { error: err } as IError;
  }
}
